#!/usr/bin/env bash

#echo Wie ist der Volume ID? Beispiel: 9789978

#read VOLUMEID

mkdir /data
mount -o discard,defaults /dev/sdb /data
echo "/dev/sdb /data ext4 discard,nofail,defaults 0 0" >> /etc/fstab

#Swap
fallocate -l 4G /swapfile
chown root:root /swapfile
chmod 0600 /swapfile
mkswap /swapfile
swapon /swapfile
echo '/swapfile none swap sw 0 0' >> /etc/fstab

ssh-keygen -o -a 100 -t ed25519 -q -N "" -f /root/.ssh/id_ed25519


apt update
apt upgrade -y
apt install docker.io jq borgmatic incron -y

generate-borgmatic-config

mkdir /data/composer
mkdir /data/composer/psc
mkdir /data/composer/letsencrypt

mkdir /data/volumes
mkdir /data/volumes/letsencrypt
mkdir /data/volumes/letsencrypt/html
mkdir /data/volumes/letsencrypt/conf
mkdir /data/volumes/letsencrypt/vhost
mkdir /data/volumes/letsencrypt/dhparam
mkdir /data/volumes/letsencrypt/certs
mkdir /data/volumes/letsencrypt/acme


mkdir /data/volumes/psc
mkdir /data/volumes/psc/templates
mkdir /data/volumes/psc/styles
mkdir /data/volumes/psc/temp
mkdir /data/volumes/psc/shops
mkdir /data/volumes/psc/packages
mkdir /data/volumes/psc/mysql
mkdir /data/volumes/psc/mongodb
mkdir /data/volumes/psc/media
mkdir /data/volumes/psc/market
mkdir /data/volumes/psc/plugins
mkdir /data/volumes/psc/uploads
mkdir /data/volumes/psc/bundles
mkdir /data/volumes/psc/watch
touch /data/volumes/psc/watch/upgrade
touch /data/volumes/psc/watch/update
chmod -R 0777 /data/volumes/psc/watch

echo '/data/volumes/psc/watch/upgrade IN_MODIFY cd /data/composer/psc && docker system prune -f && docker-compose pull && docker-compose up -d' >> /etc/incron.d/1-upgrade
echo '/data/volumes/psc/watch/update IN_MODIFY cd /data/composer/psc && docker-compose up -d' >> /etc/incron.d/2-update

curl -L "https://github.com/docker/compose/releases/download/1.28.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

docker network create nginx-proxy

curl -L "https://gitlab.com/printshopcreator/psc-per-vm/-/raw/master/data/composer/letsencrypt/docker-compose.yml" -o /data/composer/letsencrypt/docker-compose.yml
curl -L "https://gitlab.com/printshopcreator/psc-per-vm/-/raw/master/data/composer/psc/docker-compose.yml" -o /data/composer/psc/docker-compose.yml

cd /data/composer/letsencrypt
docker-compose up -d
cd /data/composer/psc
